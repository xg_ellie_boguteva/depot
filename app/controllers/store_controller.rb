class StoreController < ApplicationController
  skip_before_action :authorize
  include CurrentCart
  before_action :set_cart
  
  def index
    # @products = Product.order(:title)
    if params[:set_locale]
      redirect_to store_url(locale: params[:set_locale])
    else
      @products = Product.order(:title)
      # @cart = current_cart
    end

    # time = Time.new
    # @current_time = time.strftime("%Y-%m-%d %H:%M:%S")
  #   if session[:counter].nil?
    # session[:counter] = 1
  #   else
    # session[:counter] = session[:counter] + 1
  #   end
  #   @counter = session[:counter]
  end
end
