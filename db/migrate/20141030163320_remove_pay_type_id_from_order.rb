class RemovePayTypeIdFromOrder < ActiveRecord::Migration
  def change
    remove_column :orders, :pay_type_id, :string
  end
end
